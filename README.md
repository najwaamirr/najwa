# INTRODUCTION

### My Pic :camera:
![me_5](/uploads/39beb0951460708d39deb041e7b0dcbb/me_5.jpeg)

### About Me :raising_hand:
Hi, my name is **Najwa**. But people usually call me **wawa**. 

I'm from **Kedah**.

3 facts about me :
* I am a middle child
* I love music and sleep
* I am a shy person

### My Strength :muscle: and Weakness :weary:

|  Strength :muscle:   |  Weakness :weary:   |
|----------------------|---------------------|
| Able to work in team |   Low self-esteem   |
|    Determination     |   Hard to speak up  |

