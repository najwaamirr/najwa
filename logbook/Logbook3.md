# My Engineering Logbook 

![UPM-Logo-Vector](/uploads/a33eeb7a6595e01a02fd1c89af0c8d05/UPM-Logo-Vector.png)

## Integrated Design Project Log 


|Nama: Najwa Husna Binti Amirruddin|Matric.No:199131|Subsystem: Simulation|
|------|------|------|

|Logbook Entry: 03| Date: 19/11/2021|Week: 5|
|------|------|------|

|1. Our Agenda|
|------------|
|a) Proceed with the designing the HAU in catia (touch up).|
|b) Run simulation software using ansys.|

|2. Our Goals|
|------------|
|a) Get initial results from the simulation.|
|b) Change the requirements that needed to be in simulation.|

|3. Decision to solve the problem|
|-------|
|a) Run ansys simulation.|

|4. Method to solve the problem|
|-----------|
|a) Review few of simulations of HAU available in the internet.|

|5. Justification when solving the problem|
|------------|
|a) Discuss on what parameter that need to be considered.|
|b) Reading books and journal related the HAU system.|

|6. Impact of/after the decision chosen|
|-------------------|
|a) Have a good illustrations or imaginations on how to design the HAU in the CATIA (Airframe Design).|
|b) understand a bit how ansys simulation works.|

|7. Our next step|
|----------|
|a) Decide the real parameter to be used in the simulation soon.|
|b) Analysing data from simulation software|
|c) Do the performance calculation.|
