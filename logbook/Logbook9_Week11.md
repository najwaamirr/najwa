# My Engineering Logbook 

![UPM-Logo-Vector](/uploads/a33eeb7a6595e01a02fd1c89af0c8d05/UPM-Logo-Vector.png)

## Integrated Design Project Log 


|Nama: Najwa Husna Binti Amirruddin|Matric.No:199131|Subsystem: Simulation|
|------|------|------|

|Logbook Entry: 09| Date: 07/01/2022|Week:11|
|------|------|------|

|1. Our Agenda|
|------------|
|a) Present the simulation data of our airship.|
|b) Settle all calculation related to aerodynamic vector of airship.|

|2. Our Goals|
|------------|
|a) Calculate the aerodynamic vector and present to dr.|

|3. Decision to solve the problem|
|-------|
|a) Refering to the textbook.|
|b) Using excel so that it is representable.|

|4. Method to solve the problem|
|-----------|
|a) Discuss with subsystem member.|
|b) Gather all the parameter need to be used in the calculation.|

|5. Justification when solving the problem|
|------------|
|a) Discuss with dr if there is misunderstanding.|
|b) Produce great idea to settle the calculation.|

|6. Impact of/after the decision chosen|
|-------------------|
|a) Able to ccomplete the calculation of the aerodynamic vector.|

|7. Our next step|
|----------|
|a) Present the result data to other subsystem.|
|b) Start to write report for the subsystem.|
