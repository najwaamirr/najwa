# My Engineering Logbook 

![UPM-Logo-Vector](/uploads/a33eeb7a6595e01a02fd1c89af0c8d05/UPM-Logo-Vector.png)

## Integrated Design Project Log 


|Nama: Najwa Husna Binti Amirruddin|Matric.No:199131|Subsystem: Simulation|
|------|------|------|

|Logbook Entry: 07| Date: 24/12/2021|Week:9|
|------|------|------|

THERE IS NO CLASS BECAUSE OF 'TEMPOH BERTENANG'

|1. Our Agenda|
|------------|
| - |

|2. Our Goals|
|------------|
| - |

|3. Decision to solve the problem|
|-------|
| -  |

|4. Method to solve the problem|
|-----------|
| -  |

|5. Justification when solving the problem|
|------------|
| -  |

|6. Impact of/after the decision chosen|
|-------------------|
| -  |

|7. Our next step|
|----------|
| -  |
