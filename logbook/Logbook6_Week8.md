# My Engineering Logbook 

![UPM-Logo-Vector](/uploads/a33eeb7a6595e01a02fd1c89af0c8d05/UPM-Logo-Vector.png)

## Integrated Design Project Log 


|Nama: Najwa Husna Binti Amirruddin|Matric.No:199131|Subsystem: Simulation|
|------|------|------|

|Logbook Entry: 06| Date: 17/12/2021|Week:8|
|------|------|------|

|1. Our Agenda|
|------------|
|a) Compare the simulation result with the existing data from research paper.|

|2. Our Goals|
|------------|
|a) Evaluate our setup with the existing setup from reasearch paper.|

|3. Decision to solve the problem|
|-------|
|a) Validate our result of NACA0012.|
|b) Compare the experimental results with NACA0012 research paper.|

|4. Method to solve the problem|
|-----------|
|a) Refer to many existing NACA0012 research paper if we unsure about a thing.|
|b) Find related paper as many as we can about NACA0012 simulation.|

|5. Justification when solving the problem|
|------------|
|a) So that we can validate that our setup is correct.|
|b) Following all the parameters so that we can validate our setup.|

|6. Impact of/after the decision chosen|
|-------------------|
|a) The trend of the simulation data is similar with the reference.|
|b) We can confirm that our setup for simulation is correct.|

|7. Our next step|
|----------|
|a) Using the same setup to simulate our HAU airship.|
|b) Obtain required aerodynamic force that will be used by other subsystem.|
