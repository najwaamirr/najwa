# My Engineering Logbook 

![UPM-Logo-Vector](/uploads/a33eeb7a6595e01a02fd1c89af0c8d05/UPM-Logo-Vector.png)

## Integrated Design Project Log 


|Nama: Najwa Husna Binti Amirruddin|Matric.No:199131|Subsystem: Simulation|
|------|------|------|

|Logbook Entry: 05| Date: 03/12/2021|Week:7|
|------|------|------|

|1. Our Agenda|
|------------|
|a) Run simulation software using ansys for 2D airfoil.|
|b) Compare the simulation result with the reference book.|
|c) Provide the expected length of the airship from graph.|

|2. Our Goals|
|------------|
|a) Run smulation for 2D model.|
|b) Evaluate our setup with the reference book.|

|3. Decision to solve the problem|
|-------|
|a) Run ansys simulation.|
|b) Plot the cl vs angle of attack graph.|
|c) Plot the cd vs angle of attack graph.|


|4. Method to solve the problem|
|-----------|
|a) Review few of simulations of HAU available in the internet.|
|b) Using ansys fluent to simulate the model.|
|c) Using excel to plot the graph.|

|5. Justification when solving the problem|
|------------|
|a) Meshing setup is very important in simulation process.|
|b) Refering to the book is the best way to evaluate our simulation.|

|6. Impact of/after the decision chosen|
|-------------------|
|a) Able to compare our simulation with the simulation in the books.|
|b) Evaluate our simulation progress.|

|7. Our next step|
|----------|
|a) Improve our simulation for 3D model.|
