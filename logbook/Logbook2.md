# My Engineering Logbook 

![UPM-Logo-Vector](/uploads/a33eeb7a6595e01a02fd1c89af0c8d05/UPM-Logo-Vector.png)

## Integrated Design Project Log 


|Nama: Najwa Husna Binti Amirruddin|Matric.No:199131|Subsystem: Simulation|
|------|------|------|

|Logbook Entry: 02| Date: 12/11/2021|
|------|------|

|1. Our Agenda|
|------------|
|a) Discuss about designing the airship before run it in the simulation software.|
|b) Design the airship using any CAD software.|
|c) Start to familiarize myself with simulation software (eg.ansys) to be used later.|

|2. Our Goals|
|------------|
|a) To have the airship design by the end of this week.|
|b) To decide which design will be used in the simulation.|
|c) Calculation may start by the end of the week.|

|3. Decision to solve the problem|
|-------|
|a) Start designing the airship using Catia and Solidworks.|

|4. Method to solve the problem|
|-----------|
|a) Each of the design member come out with their own design.|
|b) Measurements: Tolerance was set according to the measurements to avoid greater significant on the error involved.|

|5. Justification when solving the problem|
|------------|
|a) To perform a prototype which looks good in terms of the measurements ratio set up in the CATIA programme.|
|b) To perform the good performance calculations or reviews on HAU model.|
|c) To have a good results on the Fluid Dynamics concept.|

|6. Impact of/after the decision chosen|
|-------------------|
|a) Have a good illustrations or imaginations on how to design the HAU in the CATIA (Airframe Design).|
|b) Simulation team able to start to do the analysis|
|c) Calculation teams can also start doing the calculation related to the analysis.|

|7. Our next step|
|----------|
|a) Analysing data from simulation software|
|b) Do the performance calculation.|
