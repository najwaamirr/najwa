# My Engineering Logbook 

![UPM-Logo-Vector](/uploads/a33eeb7a6595e01a02fd1c89af0c8d05/UPM-Logo-Vector.png)

## Integrated Design Project Log 


|Nama: Najwa Husna Binti Amirruddin|Matric.No:199131|Subsystem: Simulation|
|------|------|------|

|Logbook Entry: 05| Date: 05/10/2021|Week:2|
|------|------|------|

|1. Our Agenda|
|------------|
|a) Propose project timeline for every subsystem.|
|b) Define role for every week.|
|c) Complete gitlab activity.|

|2. Our Goals|
|------------|
|a) To have better future refering to the timeline.|
|b) Everyone able to use gitlab without any problem.|

|3. Decision to solve the problem|
|-------|
|a) Make gantt chart using excel spreadsheet.|
|b) Do tutorial to friends who didnt know how gitlab works.|

|4. Method to solve the problem|
|-----------|
|a) Discuss together with subsystem's member to create the timeline of the project.|

|5. Justification when solving the problem|
|------------|
|a) Have proper guide to complete the project.|
|b) Explore more tools on how to use gitlab.|

|6. Impact of/after the decision chosen|
|-------------------|
|a) Everyone can use gitlab.|

|7. Our next step|
|----------|
|a) Present the timeline of the subsystem to others.|
|b) Go to lab to take measurement of the HAU.|
